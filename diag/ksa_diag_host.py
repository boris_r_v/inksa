#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_pk(parent, logic, klass, hostname, os):
    style = {'stroke': 'black', 'stroke-width': '1'}
    attribs = {inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, 'style': simplestyle.formatStyle(style)}
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    style = {'fill': '#bfbfbf'}
    attribs = {'x': '9', 'y': '0', 'width': '52', 'height': '45', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    style = {'fill': '#bfbfbf'}
    attribs = {'x': '0', 'y': '49', 'width': '70', 'height': '10', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    style = {'fill': 'red'}
    attribs = {'x': '15', 'y': '6', 'width': '40', 'height': '33', inksa.prefix + 'subid': 'net', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none'}
    attribs = {'x': '59', 'y': '52', 'width': '8', 'height': '4', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    style = {'fill': 'white', 'stroke': 'white', 'text-anchor': 'middle', 'font-size': '16', 'font-weight': 'bold'}
    attribs = {'x': '35', 'y': '77',  'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = hostname

    style = {'fill': 'blue', 'stroke': 'blue', 'font-size': '12'}
    attribs = {'x': '-39', 'y': '97', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'IP:'

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '97', inksa.prefix + 'subid': 'ip', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs)

    style = {'fill': 'blue', 'stroke': 'blue', 'font-size': '12'}
    attribs = {'x': '-39', 'y': '117', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'MAC:'

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '117', inksa.prefix + 'subid': 'mac', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs)

    style = {'fill': 'blue', 'stroke': 'blue', 'font-size': '12'}
    attribs = {'x': '-39', 'y': '137', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode('ОС:')

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '137', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = os
    return group

def draw_controller(parent, logic, klass, hostname, os):
    style = {'stroke': 'black', 'stroke-width': '1'}
    attribs = {inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, 'style': simplestyle.formatStyle(style)}
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    style = {'fill': '#bfbfbf'}
    attribs = {'points': '0,0 0,59 43,59 43,54 10,54 10,0', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('polygon', 'svg'), attribs)
    
    style = {'fill': 'red'}
    attribs = {'x': '12', 'y': '4', 'width': '30', 'height': '40', inksa.prefix + 'subid': 'net', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none', 'stroke-width': '0.5' }
    attribs = {'x': '2', 'y': '2', 'width': '6', 'height': '16',  inksa.prefix + 'subid': 'beat', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)
    style = {'stroke-width': '1' }
    
    attribs = {'points': '0,20 10,20'}
    inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)
    
    attribs = {'points': '0,40 10,40'}
    inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)

    style = {'fill': 'white', 'stroke': 'white', 'text-anchor': 'middle', 'font-size': '16', 'font-weight': 'bold'}
    attribs = {'x': '25', 'y': '77', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = hostname

    style = {'fill': 'blue', 'stroke': 'blue'}
    attribs = {'x': '-39', 'y': '97', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'IP:'

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '97', inksa.prefix + 'subid': 'ip', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs)

    style = {'fill': 'blue', 'stroke': 'blue'}
    attribs = {'x': '-39', 'y': '117', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'MAC:'

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '117', inksa.prefix + 'subid': 'mac', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs)

    style = {'fill': 'blue', 'stroke': 'blue'}
    attribs = {'x': '-39', 'y': '137', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode('ОС:')

    style = {'fill': 'white', 'stroke': 'white'}
    attribs = {'x': '0', 'y': '137', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = os
    return group

class KsaDiagHost(inksa.Effect):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--hostname',
					action='store', type='string',	
					dest='hostname',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='pk',
					help='Type of host')
	self.OptionParser.add_option('--os',
					action='store', type='string',
					dest='os',
					help='Operation system')

    def effect(self):
	logic = unicode(self.options.logic)
	klass = unicode(self.options.klass)
	hostname = unicode(self.options.hostname)
	os = unicode(self.options.os)

	node = None
	if self.options.mode == 'pk':
	    node = draw_pk(self.current_layer, logic, klass, hostname, os)
	    
	elif self.options.mode == 'controller':
	    node = draw_controller(self.current_layer, logic, klass, hostname, os)
	
        attribs = {'mod':'RMS:StdLogEvent','type':'fail','name':u'АРМ А','event':u'Нет ответа по сети','note':'','link_type':"parent_attr",'link':'fail'}
	inkex.etree.SubElement( node, inkex.addNS('event', 'ksa'), attribs )
	

if __name__ == '__main__':
    e = KsaDiagHost()
    e.affect()
