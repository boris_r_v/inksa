#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_connector(parent, logic, ids):

    for i in range(1, len(ids)):
	style = {'stroke': 'red', 'marker-start': 'url(#TriangleOutL)', 'marker-end': 'url(#TriangleOutL)'}
	attribs = {
		    '{%s}connection-start' % inkex.NSS['inkscape']: '#' + ids[i - 1],
		    '{%s}connection-end' % inkex.NSS['inkscape']: '#' + ids[i],
		    '{%s}connector-type' % inkex.NSS['inkscape']: 'polyline',
		    'style': simplestyle.formatStyle(style)
	      }
	inkex.etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs, inksa.nsmap)

class KsaDiagConnector(inksa.Effect):

    def __init__(self):
	inksa.Effect.__init__(self)
	
	self.OptionParser.add_option('--width',
					action='store', type='float',
					dest='width', default=1,
					help='The number of the elements')
    def effect(self):
	if len(self.options.ids) < 2:
	    inkex.errormsg('Необходимо выбрать не менее двух элементов для соединения!')
	    exit(1)

	logic = unicode(self.options.logic)
	draw_connector(self.current_layer, logic, self.options.ids)

if __name__ == '__main__':
    e = KsaDiagConnector()
    e.affect()
