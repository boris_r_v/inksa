#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_double_board(parent, logic, klass, name, num_of_kits, tests):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass,  inksa.prefix + 'std_mnemo': u"Модуль_КТС_УК", inksa.prefix+"title":name  }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    style = {'fill': '#c0c0c8', 'stroke': '#666699'}
    attribs = {'width': '100', 'height': '120', 'style': simplestyle.formatStyle(style), inksa.prefix + 'subid': 'bound'}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    style = {'fill': 'white', 'stroke': 'white', 'text-anchor': 'middle', 'font-size': '14', 'font-weight': 'bold'}
    attribs = {'x': '50', 'y': '13', 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    if(num_of_kits == 1):
	style = {'fill': 'red', 'stroke': '#444466'}
	attribs = {'x': '10', 'y': '15', 'width': '80', 'height': '90', inksa.prefix + 'subid': 'status_o', 'style': simplestyle.formatStyle(style)}
	inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)

    elif(num_of_kits == 2):	    
	if(tests == 'true'):
	    style = {'fill': 'red', 'stroke': '#444466'}
	    attribs = {'x': '20', 'y': '15', 'width': '10', 'height': '90', inksa.prefix + 'subid': 'status_o', 'style': simplestyle.formatStyle(style)}
	    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)

	    style = {'fill': 'red', 'stroke': '#444466'}
	    attribs = {'x': '70', 'y': '15', 'width': '10', 'height': '90', inksa.prefix + 'subid': 'status_r', 'style': simplestyle.formatStyle(style)}
	    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)

	    for i in range(4):
		subid = 'test' + str(i + 1) + '_o'
		style = {'fill': 'none', 'stroke': '#444466'}
		attribs = {'x': '5', 'y': str(25 + i * 20), 'width': '10', 'height': '10', inksa.prefix + 'subid': subid, 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

		subid = 'test' + str(i + 5) + '_o'
		style = {'fill': 'none', 'stroke': '#444466'}
		attribs = {'x': '35', 'y': str(25 + i * 20), 'width': '10', 'height': '10', inksa.prefix + 'subid': subid, 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

		subid = 'test' + str(i + 1) + '_r'
		style = {'fill': 'none', 'stroke': '#444466'}
		attribs = {'x': '55', 'y': str(25 + i * 20), 'width': '10', 'height': '10', inksa.prefix + 'subid': subid, 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

		subid = 'test' + str(i + 5) + '_r'
		style = {'fill': 'none', 'stroke': '#444466'}
		attribs = {'x': '85', 'y': str(25 + i * 20), 'width': '10', 'height': '10', inksa.prefix + 'subid': subid, 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

	elif(tests == 'false'):
	    style = {'fill': 'red', 'stroke': '#444466'}
	    attribs = {'x': '10', 'y': '15', 'width': '30', 'height': '90', inksa.prefix + 'subid': 'status_o', 'style': simplestyle.formatStyle(style)}
	    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs, inksa.nsmap)

	    style = {'fill': 'red', 'stroke': '#444466'}
	    attribs = {'x': '60', 'y': '15', 'width': '30', 'height': '90', inksa.prefix + 'subid': 'status_r', 'style': simplestyle.formatStyle(style)}
	    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

	style = {'text-anchor': 'middle'}
	attribs = {'x': '25', 'y': '30', 'style': simplestyle.formatStyle(style)}
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode('О')

	style = {'text-anchor': 'middle'}
	attribs = {'x': '75', 'y': '30', 'style': simplestyle.formatStyle(style)}
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode('Р')

class KsaDiagBoard(inksa.Effect):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name',
					help='Name')
	self.OptionParser.add_option('--num_of_kits',
					action='store', type='int',
					dest='num_of_kits',
					help='Num of kits')
	self.OptionParser.add_option('--tests',
					action='store', type='string',
					dest='tests',
					help='Availability of test')

    def effect(self):
	logic = unicode("Diag:Board")
	klass = unicode("Diag:BoardClass")
	name = unicode(self.options.name)
	draw_double_board(self.current_layer, logic, klass, name, self.options.num_of_kits, self.options.tests)

if __name__ == '__main__':
    e = KsaDiagBoard()
    e.affect()
