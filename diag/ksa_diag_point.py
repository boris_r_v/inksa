#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_point(parent, logic, klass, radius):
    style = {'fill': 'black'}
    attribs = {inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, 'r': str(radius), 'style': simplestyle.formatStyle(style)}
    inkex.etree.SubElement(parent, inkex.addNS('circle', 'svg'), attribs, inksa.nsmap)

class KsaDiagPoint(inksa.Effect):

    def __init__(self):
	inksa.Effect.__init__(self)

	self.OptionParser.add_option('--radius',
					action='store', type='int',
					dest='radius', default=1,
					help='The radius of the point')

    def effect(self):
	logic = unicode(self.options.logic)
	klass = unicode(self.options.klass)
	draw_point(self.current_layer, logic, klass, self.options.radius)

if __name__ == "__main__":
    e = KsaDiagPoint()
    e.affect()
