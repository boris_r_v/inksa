#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_converter(parent, logic, klass, input_mode, output_mode, num_elements):
    style = {'fill': 'white', 'stroke': 'black'}
    attribs = {inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, 'style': simplestyle.formatStyle(style)}
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    for i in range (num_elements - 1, 0, -1):
	attribs = {'x': str(i * 3), 'y': str(i * 3), 'width': '50', 'height': '50'}
	inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    attribs = {'x': '0', 'y': '0', 'width': '50', 'height': '50'}
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    attribs = {'points': '0,50 50,0'}
    inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)

    if input_mode == "constant":
	attribs = {'points': '7,10 27,10'}
	inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)
    elif input_mode == "variable":
	attribs = {'points': '7,10 10,6 11,5.2 11.5,5.1 12,5 12.5,5.1 13,5.2 14,6 17,10 20,14 21,14.8 21.5,14.9 22,15 22.5,14.9 23,14.8 24,14 27,10'}
	inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)

    if output_mode == "constant":
	attribs = {'points': '23,40 43,40'}
	inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)
    elif output_mode == "variable":
	attribs = {'points': '23,40 26,36 27,35.2 27.5,35.1 28,35 28.5,35.1 29,35.2 30,36 33,40 36,44 37,44.8 37.5,44.9 38,45 38.5,44.9 39,44.8 40,44 43,40'}
	inkex.etree.SubElement(group, inkex.addNS('polyline', 'svg'), attribs)

class KsaDiagConverter(inksa.Effect):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--input_mode',
					action='store', type='string',
					dest='input_mode', default='constant',
					help='Type of input value')
	self.OptionParser.add_option('--output_mode',
					action='store', type='string',
					dest='output_mode', default='constant',
					help='Type of output value')
	self.OptionParser.add_option('--num_elements',
					action='store', type='int',
					dest='num_elements', default=1,
					help='The number of the elements')

    def effect(self):
	logic = unicode(self.options.logic)
	klass = unicode(self.options.klass)
	draw_converter(self.current_layer, logic, klass, self.options.input_mode, self.options.output_mode, self.options.num_elements)

if __name__ == '__main__':
    e = KsaDiagConverter()
    e.affect()
