#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_crossing(parent, logic, klass, name, mode ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Переезд" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_left','d':'m 45,-3.9999996 10,10 L 55,46 45,56', 'style': 'fill:none;stroke:#000000;stroke-width:5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_right','d':'M 75,-3.9999996 65,6.0000008 65,46 75,56', 'style': 'fill:none;stroke:#000000;stroke-width:5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    
    if "not_contr" != mode:
	attribs = { inksa.prefix + 'subid': '_poezd','d':'m 35,-29 0,-19.999997 25,0 L 60,-29 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_poezd_txt', 'x': '40', 'y': '-33', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;text-anchor:start;fill:#bbbbbb;stroke:#bbbbbb;stroke-width:0' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'П'
	
	attribs = { inksa.prefix + 'subid': '_hand','d':'m 60,-29 0,-19.999997 25,0 L 85,-29 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_hand_txt', 'x': '68', 'y': '-33', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;text-anchor:start;fill:#bbbbbb;stroke:#bbbbbb;stroke-width:0' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'Р'

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '60', 'y': '-16', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = { inksa.prefix + 'subid': '_fail', 'x': '54', 'y': '0', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;text-anchor:start;fill:#ff0000;stroke:#ff0000;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'A'

    attribs = { 'cx': '30', 'cy': '51', 'r':'5', inksa.prefix + 'title': 'LINK_CROSSING_ONE', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '90', 'cy': '51', 'r':'5', inksa.prefix + 'title': 'LINK_CROSSING_TWO', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_border_left', 'd': 'm 43.559269,8.9983139 6.465878,0 0,34.3766521 -6.465878,0', 'style': 'fill:none;stroke:#ff1100;stroke-width:2px' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs).text = name

    attribs = { inksa.prefix + 'subid': '_border_right', 'd': 'm 76.472597,9.0024608 -6.465878,0 0,34.3766522 6.465878,0', 'style': 'fill:none;stroke:#ff1100;stroke-width:2px' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs).text = name


class KsaRpcCrossing( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:Crossing")
	klass = unicode("RPC:CrossingClass")
	mode = unicode(self.options.mode)

	#if self.options.mode == 'not_contr':
	#    klass = unicode("RPC:CrossingClassNotControlled")
	
	    
	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_crossing( layer, logic, klass, name, mode )

if __name__ == '__main__':
    e = KsaRpcCrossing()
    e.affect()
