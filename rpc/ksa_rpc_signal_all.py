#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_simple(parent, logic, klass, name, signal_type, uni_type ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'uni_type': uni_type, inksa.prefix + 'signal_type': signal_type, inksa.prefix + 'std_mnemo': u"Сигнал" }
    if "6" == uni_type:	#для перегонного светофора - уменьшим начертание светофора
	attribs['transform'] = "scale(0.7,0.7)"
    if "5" == uni_type:	#для заградительного светофора - уменьшим начертание светофора
	attribs['transform'] = "scale(0.7,0.7)"

    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_leg', 'points': '54,30 54,10 49,10 49,18 45,18 45,22 49,22 49,30','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('polygon', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_g', 'cx': '34', 'cy':'20', 'r':'11', 'style': 'fill:none;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = {'x': '58', 'y': '27', 'text-anchor': 'start', inksa.prefix + 'subid': '_txtb', 'style': 'font-size:18px;font-weight:normal;fill:#000000;stroke:#000000;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = {'x': '7', 'y': '26', 'text-anchor': 'start', inksa.prefix + 'subid': '_txtr', 'style': 'font-size:18px;font-weight:normal;fill:#ff0000;stroke:#ff0000;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = "A"

    attribs = { 'cx': '29', 'cy': '20', 'r':'5', inksa.prefix + 'title': 'LINK_SIGNAL_OUTPUT', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '39', 'cy': '20', 'r':'5', inksa.prefix + 'title': 'LINK_SIGNAL_INPUT', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaRpcSignal( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='signal_name', 
					help='???')

	self.OptionParser.add_option('--stype',
					action='store', type='string',	
					dest='signal_type', 
					help='???')
	self.OptionParser.add_option('--unitype',
					action='store', type='string',	
					dest='uni_type', 
					help='???')

    def effect(self):
	signal_name = unicode(self.options.signal_name)

	logic = unicode("RPC:Signal")
	klass = unicode("RPC:SignalClass")
	signal_type = unicode(self.options.signal_type) 
	uni_type = unicode(self.options.uni_type) 
	
	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_simple( layer, logic, klass, signal_name, signal_type, uni_type )

if __name__ == '__main__':
    e = KsaRpcSignal()
    e.affect()
