#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_simple(parent, logic, klass, name, sect_name ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'sect_name': sect_name, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Стрелка" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main', 'd': 'M 25,106 L 66,106 L 71,116 L 25,116 Z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_psmall', 'd': 'M 68,110 L 69,112 L 147,112 L 147,110 Z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_msmall', 'd': 'M 104,38 L 68,110 L 69,112 L 105,40 L 147,40 L 147,38 Z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_upsmall', 'd': 'M 68,110 L 69,112 L 147,112 L 147,110 Z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_umsmall', 'd': 'M 104,38 L 68,110 L 69,112 L 105,40 L 147,40 L 147,38 Z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pbound', 'd': 'M 66,106 L 76,106 L 137,94 L 147,94 L 147,128 L 137,128 L 71,116 Z','style': 'fill:none;stroke:#008000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_mbound', 'd': 'M 66,106 L 76,106 L 113,56 L 147,56 L 147,22 L 96,22 Z','style': 'fill:none;stroke:#008000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_p', 'd': 'M 147,106 L 66,106 L 71,116 L 147,116 Z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_m', 'd': 'M 147,34 L 102,34 L 66,106 L 71,116 L 107,44 L 147,44 Z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_maket', 'd': 'M 59,96 L 89,96 L 89,126 L 59,126 Z','style': 'fill:none;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_plus', 'd': 'M 92,98 L 106,98','style': 'fill:none;stroke:#000000;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '43', 'y': '96', 'text-anchor': 'middle', inksa.prefix + 'subid': '_txtb', 'style': 'font-size:17px;font-weight:normal;fill:#000000;stroke:#000000;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = { 'cx': '140', 'cy': '111', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_PLUS', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '140', 'cy': '39', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_MINUS', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '32', 'cy': '111', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_COMMON', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)



class KsaRpcSwitch( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='sw_N', 
					help='???')
	self.OptionParser.add_option('--sect',
					action='store', type='string',	
					dest='sect_N', 
					help='???')

    def effect(self):
	swp_name = unicode(self.options.sw_N)
	sect_name = unicode(self.options.sect_N)
	if sect_name == '*':
	    sect_name = swp_name+"СП"
	    
	logic = unicode("RPC:SwitchPoint")
	klass = unicode("RPC:SwitchPointClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_simple( layer, logic, klass, swp_name, sect_name )

if __name__ == '__main__':
    e = KsaRpcSwitch()
    e.affect()
