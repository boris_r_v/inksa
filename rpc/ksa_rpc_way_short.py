#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Путь" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main','d':'M60,22 L 0,22 L 0,32 L 60,32 Z', 'style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { 'cx': '8', 'cy': '27', 'r':'5', inksa.prefix + 'title': 'LINK_WAY_ONE', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '52', 'cy': '27', 'r':'5', inksa.prefix + 'title': 'LINK_WAY_TWO', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txtw', 'x': '30', 'y': '16', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name



class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:Way")
	klass = unicode("RPC:WayClass")

	if self.options.mode == 'not_contr':
	    klass = unicode("RPC:WayClassNotControlled")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name)

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
