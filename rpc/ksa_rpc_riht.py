#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Смена_направлений" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_nodir','d':'m 14,17.362182 20,0 0,8.000001 -20,0 z', 'style': 'fill:none;stroke:blue;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_auto','d':'m 8,12.362183 32,0 0,18 -32,0 z', 'style': 'fill:none;stroke:blue;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_right','d':'m 8,17.362182 20,0 0,-5 12,9.000001 -12,9 0,-5 -20,0 z', 'style': 'fill:none;stroke:blue;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_left','d':'m 40,17.362182 -20,0 0,-5 -12,9.000001 12,9 0,-5 20,0 z', 'style': 'fill:none;stroke:blue;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_right_border','d':'m 8,17.362182 20,0 0,-5 12,9.000001 -12,9 0,-5 -20,0 z', 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_left_border','d':'m 40,17.362182 -20,0 0,-5 -12,9.000001 12,9 0,-5 20,0 z', 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"left", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"right", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"bound", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)



class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:Ab")
	klass = unicode("RPC:AbClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name)

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
