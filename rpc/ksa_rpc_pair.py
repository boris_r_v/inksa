#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_simple(parent, logic, klass, name, sect_name ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'sect_name': sect_name, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Стрелка", "transform": "matrix(1,0,0,-1,-20,143.36218)" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main', 'd': 'm 25,106 41,0 5,10 -46,0 z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_psmall', 'd': 'm 68,110 1,2 78,0 0,-2 z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_msmall', 'd': 'M 85,75.999997 68,110 l 1,2 17,-34.000003 z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_upsmall', 'd': 'm 68,110 1,2 78,0 0,-2 z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_umsmall', 'd': 'M 85,75.999997 68,110 l 1,2 17,-34.000003 z','style': 'fill:#bfbfbf;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pbound', 'd': 'm 66,106 10,0 61,-12 10,0 0,34 -10,0 -66,-12 z','style': 'fill:none;stroke:#008000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_mbound', 'd': 'm 66,106 10,0 16,-15.000003 -15,-30 z','style': 'fill:none;stroke:#008000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_p', 'd': 'm 147,106 -81,0 5,10 76,0 z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_m', 'd': 'M 83,71.999997 66.03125,106.07532 71,115.99329 88,81.999997 z','style': 'fill:#bfbfbf;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_maket', 'd': 'm 59,96 30,0 0,30 -30,0 z','style': 'fill:none;stroke:#bfbfbf;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_plus', 'd': 'm 92,98 14,0','style': 'fill:none;stroke:#000000;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '43', 'y': '-83', 'text-anchor': 'middle', inksa.prefix + 'subid': '_txtb', 'style': 'font-size:17px;font-weight:normal;fill:#000000;stroke:#000000;stroke-width:0', "transform":"scale(1,-1)" }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = { 'cx': '140', 'cy': '111', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_PLUS', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '140', 'cy': '39', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_MINUS', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1", "transform":"translate(-60,49)" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '32', 'cy': '111', 'r':'5', inksa.prefix + 'title': 'LINK_SWITCH_COMMON', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)



class KsaRpcSwitch( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='sw_N', 
					help='???')
	self.OptionParser.add_option('--sect',
					action='store', type='string',	
					dest='sect_N', 
					help='???')

    def effect(self):
	swp_name = unicode(self.options.sw_N)
	sect_name = unicode(self.options.sect_N)
	if sect_name == '*':
	    sect_name = swp_name+"СП"
	    
	logic = unicode("RPC:SwitchPoint")
	klass = unicode("RPC:SwitchPointClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_simple( layer, logic, klass, swp_name, sect_name )

if __name__ == '__main__':
    e = KsaRpcSwitch()
    e.affect()
