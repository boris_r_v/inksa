#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_joint(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Негабаритный_cтык", "transform":"translate(354.25873,-121.27545)" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_joint','d':'m -312.88388,210.46967 20,0 -10,0 0,20 -10,0 20,0', 'style': 'fill:none;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    
    attribs = { 'cx': '-303.1', 'cy': '220.5', 'r':'17', inksa.prefix + 'title': '_joint_circle', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#111111;stroke-opacity:0.61811069" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)
    
    attribs = { 'cx': '-320.5', 'cy': '242.6', 'r':'5', inksa.prefix + 'title': 'LINK_JOIT_ONE', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '-285.4', 'cy': '242.5', 'r':'5', inksa.prefix + 'title': 'LINK_JOINT_TWO', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)



class KsaRpcJoint( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:Joint")
	klass = unicode("RPC:jointClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_joint( layer, logic, klass, name)

if __name__ == '__main__':
    e = KsaRpcJoint()
    e.affect()
