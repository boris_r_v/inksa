#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_border(parent, logic, klass, name ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Ограждение", "transform":"translate(-55,80.000003)" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_left','d':'m 50,16 5,0 0,20 -5,0', 'style': 'fill:none;stroke:#000000;stroke-width:3px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_right','d':'m 75,16 -5,0 0,20 5,0', 'style': 'fill:none;stroke:#000000;stroke-width:3px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    

class KsaRpcBorder( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)

	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:Border")
	klass = unicode("RPC:BorderClass")
	    
	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_border( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaRpcBorder()
    e.affect()
