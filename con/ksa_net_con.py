#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle
from ksa_connector import KsaConnector

def draw_net_con(parent, logic, source, dest, svg_id, ids):
    for i in ids:
	ksa_attrs = {
			's': source,
			'd': svg_id + '@' + i + '.' + dest
	}
	style = {
			'stroke': 'blue',
			'marker-end': 'url(#KsaUCMarker)'
	}
	attribs = {
			'{%s}logic' % inkex.NSS['ksa']: logic,
			'{%s}mode' % inkex.NSS['ksa']: 'service',
			'{%s}attrs' % inkex.NSS['ksa']: unicode(simplestyle.formatStyle(ksa_attrs)),
			'{%s}connection-end' % inkex.NSS['inkscape']: '#' + i,
			'{%s}connector-type' % inkex.NSS['inkscape']: 'polyline',
			'style': simplestyle.formatStyle(style)
	}
	inkex.etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs, inkex.NSS)


class KsaNetCon(KsaConnector):

    def __init__(self):
	KsaConnector.__init__(self)
	

    def effect(self):
	if len(self.options.ids) < 1:
	    inkex.errormsg('Для этого соединения необходимо выбрать не менее одного элемента!')
	    exit(1)
	
	self.load_markers()
	logic = unicode(self.options.logic)
	source = unicode(self.options.source)
	dest = unicode(self.options.dest)
	layer = inksa.service_layer(self.current_layer, 'netcon')
	draw_net_con(layer, logic, source, dest, self.document.getroot().get('id'), self.options.ids)


if __name__ == '__main__':
    e = KsaNetCon()
    e.affect()
