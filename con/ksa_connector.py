#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

class KsaConnector(inksa.EffectBase):

    def __init__(self):
	inksa.EffectBase.__init__(self)

	self.OptionParser.add_option('--source',
					action='store', type='string',
					dest='source',
					help='The attribute source')

	self.OptionParser.add_option('--dest',
					action='store', type='string',
					dest='dest',
					help='The attribute destination')

	self.OptionParser.add_option('--width',
					action='store', type='float',
					dest='width', default=1,
					help='The number of the elements')

