#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle
from ksa_connector import KsaConnector

def draw_util_con(parent, logic, source, dest, svg_id, ids):
    
    for i in range(1, len(ids)):
	ksa_attrs = {
		    's': svg_id + '@' + ids[i - 1] + '.' + source,
		    'd': svg_id + '@' + ids[i] + '.' + dest
	}
	style = {   
		    'stroke': 'red',
		    'marker-start': 'url(#KsaUCMarker)',
		    'marker-end': 'url(#KsaUCMarker)'
	}
	attribs = { 
		    '{%s}logic' % inkex.NSS['ksa']: logic,
		    '{%s}mode' % inkex.NSS['ksa']: 'service',
		    '{%s}attrs' % inkex.NSS['ksa']: unicode(simplestyle.formatStyle(ksa_attrs)),
		    '{%s}connection-start' % inkex.NSS['inkscape']: '#' + ids[i - 1],
		    '{%s}connection-end' % inkex.NSS['inkscape']: '#' + ids[i],
		    '{%s}connector-type' % inkex.NSS['inkscape']: 'polyline',
		    'style': simplestyle.formatStyle(style)
	}
	inkex.etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs, inkex.NSS)


class KsaUtilCon(KsaConnector):

    def __init__(self):
	KsaConnector.__init__(self)

    def effect(self):
	if len(self.options.ids) < 2:
	    inkex.errormsg('Для этого соединения необходимо выбрать не менее двух элементов!')
	    exit(1)
	
	self.load_markers()
	logic = unicode(self.options.logic)
	source = unicode(self.options.source)
	dest = unicode(self.options.dest)
	layer = inksa.service_layer(self.current_layer, 'con')
	draw_util_con(layer, logic, source, dest, self.document.getroot().get('id'), self.options.ids)
	

if __name__ == '__main__':
    e = KsaUtilCon()
    e.affect()
