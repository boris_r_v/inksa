#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_prot_con(parent, logic, prop, value, note, svg_id, ids):
    for i in ids:
	ksa_attrs = {
			'who': svg_id + '@' + i,
			'prop': prop,
			'value': value,
			'note': note
	}
	style = {
			'stroke': 'green',
			'marker-start': 'url(#KsaUCMarker)',
			'marker-end': 'url(#KsaUCMarker)'
	}
	attribs = {
			'{%s}logic' % inkex.NSS['ksa']: logic,
			'{%s}mode' % inkex.NSS['ksa']: 'service',
			'{%s}attrs' % inkex.NSS['ksa']: unicode(simplestyle.formatStyle(ksa_attrs)),
			'{%s}connection-start' % inkex.NSS['inkscape']: '#' + i,
			'{%s}connector-type' % inkex.NSS['inkscape']: 'polyline',
			'style': simplestyle.formatStyle(style)
	}
	inkex.etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs, inkex.NSS)

class KsaProtCon(inksa.EffectBase):

    def __init__(self):
	inksa.EffectBase.__init__(self)
	
	self.OptionParser.add_option('--prop',
					action='store', type='string',
					dest='prop',
					help='The attribute')
	self.OptionParser.add_option('--value',
					action='store', type='string',
					dest='value',
					help='The value')
	self.OptionParser.add_option('--note',
					action='store', type='string',
					dest='note',
					help='The note')
    
    def load_markers(self):
	defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs == None:
            defs = inkex.etree.SubElement(self.document.getroot(), inkex.addNS('defs','svg'))

	mark_id = 'KsaUCMarker'
	for child in defs:
	    if child.get('id') == mark_id:
		return

	attrib = {  'id': mark_id,
		    'orient': 'auto',
		    'refX': '0.0',
		    'refY': '0.0',
		    'style': 'overflow:visible'
	}
	marker = inkex.etree.SubElement(defs, inkex.addNS('marker','svg'), attrib)

	style = {   
		    'fill-rule': 'evenodd',
		    'fill': '#ffffff',
		    'stroke': '#000000',
		    'stroke-width': '1.0pt'
	}
	attribs = {  
		    'd': 'M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z',
		    'transform': 'scale(0.8)',
		    'style': simplestyle.formatStyle(style)
	}
	inkex.etree.SubElement(marker, inkex.addNS("path", 'svg'), attribs)
    
    def effect(self):
	if len(self.options.ids) < 1:
	    inkex.errormsg('Для этого соединения необходимо выбрать не менее одного элемента!')
	    exit(1)
	
	self.load_markers()
	logic = unicode(self.options.logic)
	prop = unicode(self.options.prop)
	value = unicode(self.options.value)
	note = unicode(self.options.note)
	layer = inksa.service_layer(self.current_layer, 'protcon')
	draw_prot_con(layer, logic, prop, value, note, self.document.getroot().get('id'), self.options.ids)


if __name__ == '__main__':
    e = KsaProtCon()
    e.affect()
