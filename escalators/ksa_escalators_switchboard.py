#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_base','d':'m 0,0 0,110 311,0 0,-110 z', 'style': 'fill:#55557c;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '120', 'y': '40', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'ШУ'+name

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШУ'+name, inksa.prefix + 'std_mnemo': u"ЩУ ИОБ" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 20,60 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 20,60 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '25', 'y': '88', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs).text = unicode("ИОБ")

    attribs = {inksa.prefix + "src":u"Ошибка485Иск"+name, inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Не отвечает по RS-485", inksa.prefix + "type":"fail", inksa.prefix + "name":u"ШУ"+name+" ИОБ", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШУ'+name, inksa.prefix + 'std_mnemo': u"ЩУ ПКС" }
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 117,60 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 117,60 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '124', 'y': '88', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('text', 'svg'), attribs).text = unicode("ПКС")

    attribs = {inksa.prefix + "src":u"Ошибка485ПКС"+name, inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Не отвечает по RS-485", inksa.prefix + "type":"fail", inksa.prefix + "name":u"ШУ"+name+" ПКС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШУ'+name, inksa.prefix + 'std_mnemo': u"ЩУ КПП" }
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 214,60 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 214,60 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '219', 'y': '88', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('text', 'svg'), attribs).text = unicode("КПП")

    attribs = {inksa.prefix + "src":u"!"+name+"КИА", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Откючение АВ", inksa.prefix + "type":"event", inksa.prefix + "name":u"ШУ"+name+" КПП", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group3, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
