#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,50 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,92 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_text', 'x': '272', 'y': '121', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode("Ua, В")

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,134 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_text', 'x': '272', 'y': '163', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode("Ub, В")

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,176 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_text', 'x': '272', 'y': '205', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode("Uc, В")

    attribs = { inksa.prefix + 'subid': '_back','d':'m 380,50 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_text', 'x': '415', 'y': '79', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode("Ввод 1")

    attribs = { inksa.prefix + 'subid': '_back','d':'m 530,50 0,42 150,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_text', 'x': '565', 'y': '79', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode("Ввод 2")

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод1', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод1, Ua"}
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 380,92 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '417', 'y': '124', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns1.U1A"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод1', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод1, Ub"}
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 380,134 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '417', 'y': '166', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns1.U1B"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод1', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод1, Uc"}
    group4 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 380,176 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '417', 'y': '208', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns1.U2A"}
    inkex.etree.SubElement(group4, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод2', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод2, Ua"}
    group5 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 530,92 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group5, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '567', 'y': '124', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group5, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns2.U1A"}
    inkex.etree.SubElement(group5, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод2', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод2, Ub"}
    group6 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 530,134 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group6, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '567', 'y': '166', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group6, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns2.U1B"}
    inkex.etree.SubElement(group6, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix+"max":"399.0", inksa.prefix+"min":"342.0", inksa.prefix+'title':u'Ввод2', inksa.prefix+"alfa":"1.0", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"Ввод2, Uc"}
    group7 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 530,176 0,42 150,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group7, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '567', 'y': '208', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group7, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":"uns2.U2A"}
    inkex.etree.SubElement(group7, inkex.addNS('con', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	logic = unicode("Power:amper")
	klass = unicode("Power:amperClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
