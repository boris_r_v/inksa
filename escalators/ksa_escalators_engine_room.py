#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_base','d':'m 0,0 0,160 1445,0 0,-160 z', 'style': 'fill:#576957;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '554', 'y': '32', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode("Машинный зал эскалаторов")

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'Машинный зал', inksa.prefix + 'std_mnemo': u"Машинный зал КОД" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 20,77 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 20,77 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '25', 'y': '105', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs).text = unicode("КОД")

    attribs = {inksa.prefix + "src":u"!КОДМз", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Дверь открыта", inksa.prefix + "type":"fail", inksa.prefix + "name":u"МАШЗАЛ", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
