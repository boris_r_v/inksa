#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Эскалатор" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_base','d':'m 20,50 0,240 60,0 0,-240 z', 'style': 'fill:#959595;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_up_dummy', 'cx': '50', 'cy':'50', 'r':'30', 'style': 'fill:#959595;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_down_dummy', 'cx': '50', 'cy':'290', 'r':'30', 'style': 'fill:#959595;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_up','d':'m 0,50 100,0 -50,-50 z', 'style': 'fill:#959595;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_down','d':'m 0,290 100,0 -50,50 z', 'style': 'fill:#959595;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_foreground','d':'m 23,48 0,244 54,0 0,-244 z', 'style': 'fill:#959595;stroke:none;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_repairBack', 'cx': '50', 'cy':'200', 'r':'20', 'style': 'fill:#959595;stroke:#656565;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = {'x': '41', 'y': '213', 'text-anchor': 'start', inksa.prefix + 'subid': '_repairText', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode("Р")

    attribs = { inksa.prefix + 'subid': '_back','d':'m 36,134 0,33 26,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 36,134 0,33 26,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '39', 'y': '163', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = { inksa.prefix + 'subid': '_backDU','d':'m 21,350 0,33 58,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frameDU','d':'m 21,350 0,33 58,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '29', 'y': '379', 'text-anchor': 'start', inksa.prefix + 'subid': '_textDU', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode("ТУ")

    attribs = {inksa.prefix + "src":"!"+name+"РПВ and !"+name+"РПН", inksa.prefix + "dst":"nodir", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+"РПВ and !"+name+"РПН", inksa.prefix + "dst":"up", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":"!"+name+"РПВ and "+name+"РПН", inksa.prefix + "dst":"down", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+"РГТ", inksa.prefix + "dst":"ready", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+"ДУ", inksa.prefix + "dst":"readyDU", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"nodir", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Остановка", inksa.prefix + "type":"event", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"up", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Работа на подъем", inksa.prefix + "type":"event", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"down", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Работа на спуск", inksa.prefix + "type":"event", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"ready", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Готовность к пуску", inksa.prefix + "type":"event", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"readyDU", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Режим ДУ", inksa.prefix + "type":"event", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:EscalatorClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
