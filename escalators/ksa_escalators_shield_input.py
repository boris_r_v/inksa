#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name1, name2):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_base','d':'m 0,0 0,130 345,0 0,-130 z', 'style': 'fill:#55557c;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '33', 'y': '40', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode("ШВ")

    attribs = {'x': '150', 'y': '40', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'Ф.'+name1

    attribs = {'x': '250', 'y': '40', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'Ф.'+name2

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШВ', inksa.prefix + 'std_mnemo': u"Щит ввода" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 20,70 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 20,70 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '25', 'y': '98', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs).text = unicode("КПП")

    attribs = {inksa.prefix + "src":u"!5КИА", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Откючение АВ", inksa.prefix + "type":"fail", inksa.prefix + "name":u"ШВ КПП", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'В1', inksa.prefix + 'std_mnemo': u"Фидер1" }
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 179,103 0,57 z', 'style': 'fill:#1e558e;stroke:#1e558e;stroke-width:4' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pt', 'cx': '179', 'cy':'160', 'r':'3', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 150,70 0,33 58,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 150,70 0,33 58,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '160', 'y': '98', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('text', 'svg'), attribs).text = unicode("В1")

    attribs = {inksa.prefix + "src":u"РКН1", inksa.prefix + "dst":"v1", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"v1", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Наличие напряжения", inksa.prefix + "type":"event", inksa.prefix + "name":u"ШВ Ввод1", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'В2', inksa.prefix + 'std_mnemo': u"Фидер2" }
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 289,103 0,87 z', 'style': 'fill:#8e1e1e;stroke:#8e1e1e;stroke-width:4' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pt', 'cx': '289', 'cy':'190', 'r':'3', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 260,70 0,33 58,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 260,70 0,33 58,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '269', 'y': '98', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:#454545;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('text', 'svg'), attribs).text = unicode("В2")

    attribs = {inksa.prefix + "src":u"РКН2", inksa.prefix + "dst":"v2", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"v2", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Наличие напряжения", inksa.prefix + "type":"event", inksa.prefix + "name":u"ШВ Ввод2", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group3, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name1',
					action='store', type='string',
					dest='name1',
					help='Hostname')
	self.OptionParser.add_option('--name2',
					action='store', type='string',
					dest='name2',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name1 = unicode(self.options.name1)
	name2 = unicode(self.options.name2)
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name1, name2 )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
