#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'Шина', inksa.prefix + 'std_mnemo': u"Шина синяя"}
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 897,255 669,0 z', 'style': 'fill:#1e558e;stroke:#1e558e;stroke-width:4' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pt', 'cx': '1120', 'cy':'255', 'r':'3', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pt', 'cx': '1343', 'cy':'255', 'r':'3', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = {inksa.prefix + "src":u"РКН1", inksa.prefix + "dst":"v1", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
