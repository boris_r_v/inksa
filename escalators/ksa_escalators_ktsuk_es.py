#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass):
    group = inkex.etree.SubElement(parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_base','d':'m 0,0 0,160 345,0 0,-160 z', 'style': 'fill:#55557c;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК ГРУ" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = {'x': '70', 'y': '95', 'text-anchor': 'start', inksa.prefix + 'subid': '_text_a', 'style': 'font-size:40px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs).text = unicode("О")

    attribs = {'x': '170', 'y': '95', 'text-anchor': 'start', inksa.prefix + 'subid': '_text_b', 'style': 'font-size:40px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs).text = unicode("Р")

    attribs = {inksa.prefix + "src":u"ГРУ", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"!ГРУ", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Активен осн. комплект", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК О", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"ГРУ", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Активен рез. комплект", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК Р", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК ЭС" }
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 20,20 0,33 170,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 20,20 0,33 170,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '26', 'y': '48', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('text', 'svg'), attribs).text = unicode("КТС УК ЭС")

    attribs = {inksa.prefix + "src":u"К62=основной or К62=резервный or К63=основной or К63=резервный or У42=основной or У42=резервный or 2126=основной or 2126=резервный or uns1 or uns2 or uns3 or uns4", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Устройства не отвечают", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"К62=основной", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата К62о не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"К62=резервный", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата К62р не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"К63=основной", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата К63о не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"К63=резервный", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата К63р не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"У42=основной", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата У42о не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"У42=резервный", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Плата У42р не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"2126=основной", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Конвертер 21/26о не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"2126=резервный", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"Конвертер 21/26р не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"uns1", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"УНС1 не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"uns2", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"УНС2 не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"uns3", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"УНС3 не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":u"uns4", inksa.prefix + "link_type":"ts", inksa.prefix + "note":"", inksa.prefix + "event":u"УНС4 не отвечает", inksa.prefix + "type":"event", inksa.prefix + "name":u"КТС УК ЭС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК КОД" }
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 20,110 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 20,110 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '25', 'y': '138', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('text', 'svg'), attribs).text = unicode("КОД")

    attribs = {inksa.prefix + "src":u"!ОХР", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Дверь открыта", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК КОД", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group3, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК КПП" }
    group4 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 140,110 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 140,110 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group4, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '145', 'y': '138', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('text', 'svg'), attribs).text = unicode("КПП")

    attribs = {inksa.prefix + "src":u"КППОш or КППРш", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group4, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Откючение АВ", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК КПП", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group4, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК АКБ" }
    group5 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 250,110 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group5, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 250,110 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group5, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '257', 'y': '138', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group5, inkex.addNS('text', 'svg'), attribs).text = unicode("АКБ")

    attribs = {inksa.prefix + "src":u"КИБПАБ", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group5, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Работа от батареи", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК АКБ", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group5, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК ИП1" }
    group6 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 250,20 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group6, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 250,20 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group6, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '257', 'y': '48', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group6, inkex.addNS('text', 'svg'), attribs).text = unicode("ИП1")

    attribs = {inksa.prefix + "src":u"КИП2/А", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group6, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Авария источника пит.", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК ИП1", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group6, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'КТС УК', inksa.prefix + 'std_mnemo': u"КТС УК ИП2" }
    group7 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 250,65 0,33 77,0 0,-33 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group7, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 250,65 0,33 77,0 0,-33 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group7, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '257', 'y': '93', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group7, inkex.addNS('text', 'svg'), attribs).text = unicode("ИП2")

    attribs = {inksa.prefix + "src":u"КИП2/Б", inksa.prefix + "dst":"kp", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group7, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"kp", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Авария источника пит.", inksa.prefix + "type":"fail", inksa.prefix + "name":u"КТС УК ИП2", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group7, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
