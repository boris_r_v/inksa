#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_text', 'x': '8', 'y': '32', 'style': 'font-size:35px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = u'ИОБ Эск.'+name

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'queue': u"Блокировочная цепь НОРМА,Кнопка СТОП на ШУ,КЛЮЧ СТОП ВЕРХНЯЯ КАБИНА,Кнопка СТОП на ПУВ,КЛЮЧ СТОП ВЕРХНИЙ,КЛЮЧ СТОП НИЖНИЙ,Кнопка СТОП на ПУН,РАБОЧИЙ ТОРМОЗ,Контроль взведения ЭАТ,ВИНТ АВАРИЙНОГО ТОРМОЗА,УПОР АВАРИЙНОГО ТОРМОЗА,ГАЙКА АВАРИЙНОГО ТОРМОЗА,ВХ.ПЛОЩАДКА ВЕРХНЯЯ ЛЕВАЯ,ВХ.ПЛОЩАДКА ВЕРХНЯЯ ПРАВАЯ,БЛОКИР.ДЕМОНТАЖА СТУПЕНЕЙ,СТОП-ТРОС,БЛОКИР.СТУПЕНИ ВЕРХНЯЯ ПРАВАЯ,БЛОКИР.СТУПЕНИ ВЕРХНЯЯ ЛЕВАЯ,БЛОКИРОВКА БЕГУНКОВ ПРАВАЯ,БЛОКИРОВКА БЕГУНКОВ ЛЕВАЯ,НАТЯЖКА ПРАВОГО ПОРУЧНЯ,НАТЯЖКА ЛЕВОГО ПОРУЧНЯ,СТУПЕНИ НИЖ.ЛЕВ.+ПОДЪЕМА СТУПЕНИ,БЛОКИР.СТУПЕНИ НИЖНЯЯ ПРАВАЯ,НАТЯЖКА ПРАВОЙ ЦЕПИ,НАТЯЖКА ЛЕВОЙ ЦЕПИ,ВХ.ПЛОЩАДКА НИЖНЯЯ ЛЕВАЯ,ВХ.ПЛОЩАДКА НИЖНЯЯ ПРАВАЯ,БЛОКИРОВКА СХОДА ПОРУЧНЯ,ОСТАНОВКА ЛЕВОГО ПОРУЧНЯ,ОСТАНОВКА ПРАВОГО ПОРУЧНЯ,КОНТРОЛЬ ОБРЫВА ФАЗ,КОНТРОЛЬ НАГРЕВА ПОДШИПНИКОВ,Нет данных", inksa.prefix + 'std_mnemo': u"ИОБ Эск" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 174,0 0,42 644,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '190', 'y': '30', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs ).text = unicode("Нет данных")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":u"НомерБл"+name}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"sout", inksa.prefix + "link_type":"text_attr", inksa.prefix + "note":"", inksa.prefix + "event":"", inksa.prefix + "type":"event", inksa.prefix + "name":u"ИОБ Эск."+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:QText")
	klass = unicode("RPC:QTextClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
