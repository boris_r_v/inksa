#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ТС', inksa.prefix + 'std_mnemo': u"Диаг канала ТС"}
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back', 'cx': '20', 'cy':'20', 'r':'10', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = {inksa.prefix + "src":u"!Uralskay_Escalators.ts_fault", inksa.prefix + "dst":"green_mg", inksa.prefix + "mod":"util:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":u"Uralskay_Escalators.ts_fault", inksa.prefix + "dst":"red_mg", inksa.prefix + "mod":"util:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"red_mg", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Сбой ТС", inksa.prefix + "type":"event", inksa.prefix + "name":u"Диаг канала ТС", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
