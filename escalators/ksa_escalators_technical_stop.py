#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Тех.Ост. эск" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 0,0 0,34 222,0 0,-34 z', 'style': 'fill:#959595;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 0,0 0,34 222,0 0,-34 z', 'style': 'fill:none;stroke:#656565;stroke-width:3' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '8', 'y': '29', 'text-anchor': 'start', inksa.prefix + 'subid': '_text', 'style': 'font-size:35px;font-weight:bold;fill:none;stroke:#ffffff;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = 'Тех.Ост. эск.'+name

    attribs = {inksa.prefix + "src":"!"+name+"РБО", inksa.prefix + "dst":"stop", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"stop", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Техническая остановка", inksa.prefix + "type":"fail", inksa.prefix + "name":u"Эскалатор "+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:TechnicalStopClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
