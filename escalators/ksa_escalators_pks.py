#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_text', 'x': '255', 'y': '30', 'style': 'font-size:32px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = u'ПКС Эскалатор '+name

    if ( name == '1' ):

	attribs = { inksa.prefix + 'subid': '_text', 'x': '8', 'y': '84', 'style': 'font-size:35px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode('Код "Норма"')

	attribs = { inksa.prefix + 'subid': '_text', 'x': '8', 'y': '136', 'style': 'font-size:35px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode('Код Исправно')

	attribs = { inksa.prefix + 'subid': '_text', 'x': '18', 'y': '188', 'style': 'font-size:35px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode('ПКС (скор. 1)')

	attribs = { inksa.prefix + 'subid': '_text', 'x': '18', 'y': '240', 'style': 'font-size:35px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
	inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = unicode('ПКС (скор. 2)')

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'queue': u"Нет данных,Увел. ск. на 25%,Умен. ск. на 50% подъем,Обратный ход,Нет данных,Нет данных,Нет данных,Нет данных,Самоход,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Отказ раб. торм.,Нет данных", inksa.prefix + 'std_mnemo': u"Код Норма" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,50 0,42 285,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '233', 'y': '79', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('text', 'svg'), attribs ).text = unicode("Нет данных")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":u"ПКС"+name+"_код1"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"sout", inksa.prefix + "link_type":"text_attr", inksa.prefix + "note":"", inksa.prefix + "event":"", inksa.prefix + "type":"event", inksa.prefix + "name":u"ПКС Эск."+name+" Норма", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group1, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'queue': u"Нет данных,Требуется калибровка,Умен. ск. на 50% спуск,Нет данных,Неиспр. входн. сигн.,Нет данных,Нет данных,Нет данных,Неиспр. прибора 1,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Неиспр. РКП,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Неиспр. ИПТ,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Нет данных,Торм. путь больше нормы,Нет данных", inksa.prefix + 'std_mnemo': u"Код Исправно" }
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 230,102 0,42 285,0 0,-42 z', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '233', 'y': '131', 'style': 'font-size:26px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('text', 'svg'), attribs ).text = unicode("Нет данных")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":u"ПКС"+name+"_код2"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"sout", inksa.prefix + "link_type":"text_attr", inksa.prefix + "note":"", inksa.prefix + "event":"", inksa.prefix + "type":"event", inksa.prefix + "name":u"ПКС Эск."+name+" Испр.", inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group2, inkex.addNS('event', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': "Power:amper", inksa.prefix + 'class': "Power:amperClass", inksa.prefix+"max":"100.0", inksa.prefix+"min":"0.0", inksa.prefix+'title':name, inksa.prefix+"alfa":"0.01", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"ПКС скор.1"}
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 230,154 0,42 285,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '345', 'y': '186', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":u"ПКС"+name+"_скорость1"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': "Power:amper", inksa.prefix + 'class': "Power:amperClass", inksa.prefix+"max":"100.0", inksa.prefix+"min":"0.0", inksa.prefix+'title':name, inksa.prefix+"alfa":"0.01", inksa.prefix+"betta":"0.0", inksa.prefix + 'std_mnemo': u"ПКС скор.2"}
    group4 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'm 230,206 0,42 285,0 0,-42 z','style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'txt', 'x': '345', 'y': '238', 'style': 'font-size:30px;font-weight:normal;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group4, inkex.addNS('text', 'svg'), attribs ).text = unicode("0.00")

    attribs = {inksa.prefix + "dst":"in", inksa.prefix + "mod":"Net:cons", inksa.prefix + "src":u"ПКС"+name+"_скорость2"}
    inkex.etree.SubElement(group4, inkex.addNS('con', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("RPC:QText")
	klass = unicode("RPC:QTextClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
