#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    group = inkex.etree.SubElement( parent, inkex.addNS('g','svg'))

    attribs = { inksa.prefix + 'subid': '_base','d':'m 0,0 0,180 188,0 0,-180 z', 'style': 'fill:#55557c;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {'x': '40', 'y': '85', 'text-anchor': 'start', inksa.prefix + 'subid': '_txt', 'style': 'font-size:35px;font-weight:bold;fill:#000000;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u'ШАВР'+name

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШАВР'+name, inksa.prefix + 'std_mnemo': u"ШАВР"+name+" РКН1" }
    group1 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 18,15 0,115 56,0 0,-5 0,10 0,-5 -56,0 z', 'style': 'fill:#1e558e;stroke:#1e558e;stroke-width:4' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 30,155 30,-15 0,30 z', 'style': 'fill:#1e558e;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 30,155 30,-15 0,30 z', 'style': 'fill:none;stroke:#ffffff;stroke-width:3' }
    inkex.etree.SubElement(group1, inkex.addNS('path', 'svg'), attribs)

    attribs = {inksa.prefix + "src":u"РКН1", inksa.prefix + "dst":"v1", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group1, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШАВР'+name, inksa.prefix + 'std_mnemo': u"ШАВР"+name+" РКН2" }
    group2 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 170,45 0,85 -56,0 0,-5 0,10 0,-5 56,0 z', 'style': 'fill:#8e1e1e;stroke:#8e1e1e;stroke-width:4' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_back','d':'m 128,140 0,30 30,-15 z', 'style': 'fill:#8e1e1e;stroke:none;stroke-width:1' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_frame','d':'m 128,140 0,30 30,-15 z', 'style': 'fill:none;stroke:#ffffff;stroke-width:3' }
    inkex.etree.SubElement(group2, inkex.addNS('path', 'svg'), attribs)

    attribs = {inksa.prefix + "src":u"РКН2", inksa.prefix + "dst":"v2", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group2, inkex.addNS('con', 'ksa'), attribs)

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u'ШАВР'+name, inksa.prefix + 'std_mnemo': u"ШАВР"+name+" КМ" }
    group3 = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_cord','d':'m 94,157 0,120', 'style': 'fill:#1e558e;stroke:#1e558e;stroke-width:4' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_state1','d':'m 74,127 20,30', 'style': 'fill:#000000;stroke:#000000;stroke-width:4' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_state2','d':'m 94,157 20,-30', 'style': 'fill:#000000;stroke:#000000;stroke-width:4' }
    inkex.etree.SubElement(group3, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_pt', 'cx': '94', 'cy':'160', 'r':'3', 'style': 'fill:#ffffff;stroke:#000000;stroke-width:1' }
    inkex.etree.SubElement(group3, inkex.addNS('circle', 'svg'), attribs)

    attribs = {inksa.prefix + "src":u"!"+name+"КМ1 and !"+name+"КМ2", inksa.prefix + "dst":"state1", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+u"КМ1 and !"+name+"КМ2", inksa.prefix + "dst":"state2", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":u"!"+name+"КМ1 and "+name+"КМ2", inksa.prefix + "dst":"state3", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+u"КМ1 and "+name+"КМ2", inksa.prefix + "dst":"state4", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":name+u"КМ1 and "+name+"КМ2", inksa.prefix + "dst":"cord1", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":u"РКН1 and "+name+"КМ1 and !"+name+"КМ2", inksa.prefix + "dst":"cord2", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":u"РКН2 and !"+name+"КМ1 and "+name+"КМ2", inksa.prefix + "dst":"cord3", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":u"!"+name+"КМ1 and !"+name+"КМ2", inksa.prefix + "dst":"cord4", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group3, inkex.addNS('con', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"cord2", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Питание эск. от В1", inksa.prefix + "type":"event", inksa.prefix + "name":u"ШАВР"+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group3, inkex.addNS('event', 'ksa'), attribs)

    attribs = {inksa.prefix + "link":"cord3", inksa.prefix + "link_type":"parent_attr", inksa.prefix + "note":"",inksa.prefix + "event":u"Питание эск. от В2", inksa.prefix + "type":"event", inksa.prefix + "name":u"ШАВР"+name, inksa.prefix + "mod":"RMS:StdLogEvent"}
    inkex.etree.SubElement(group3, inkex.addNS('event', 'ksa'), attribs)


class KsaEscalators( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',
					dest='name',
					help='Hostname')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	logic = unicode("util:EmptyLogic")
	klass = unicode("util:ShieldInputClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name )

if __name__ == '__main__':
    e = KsaEscalators()
    e.affect()
