#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_simple(parent, logic, klass, name, mtype, gen_ts, title, place ):

    ts = name
    if ( len(title) ):
        ts = title

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'name': name, inksa.prefix + 'place': place, inksa.prefix + 'uni_type': '101',  inksa.prefix + 'std_mnemo':u"Измерительный_модуль"  }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = {'x': '0', 'y': '0', 'width': '40', 'height': '30', inksa.prefix + 'subid': '_bound', 'style': 'fill:none;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

    attribs = {'x': '20', 'y': '10', 'text-anchor': 'middle', inksa.prefix + 'subid': '_txt', 'style': 'font-size:10px;font-style:normal;font-weight:normal;letter-spacing:0px;word-spacing:0px;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:#000000' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = ts

    attribs = { 'cx': '5', 'cy': '-6', 'r':'5', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_link','d':'m 20,0 0,-15', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    
    if gen_ts:
        attribs = {inksa.prefix + "src": (ts+" and "+ ts +"_1"), inksa.prefix + "dst":"fail", inksa.prefix + "mod":"Net:con"}
	inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)
        
    if  "ak" == mtype:
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 5,22 10,0 10,-5', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m2','d':'m 25,22 10,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m3','d':'m 27,25 0,-10 -14,0 0,10 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	

    if  "ion" == mtype:
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 9,20 10,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m2','d':'m 21,20 10,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m3','d':'m 19,17 0,6', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m4','d':'m 21,15 0,10', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m5','d':'m 35,47.5 a 7.5,7.5 0 1 1 -15,0 7.5,7.5 0 1 1 15,0 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1', "transform":"matrix(0.9,0,0,0.93333333,-5,-24.333333)" }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    if  "uns" == mtype:
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 14,13 0,15 15,-8 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m2','d':'M 14,18 9,18', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m3','d':'M 14,23 9,23', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m4','d':'m 29,20 5,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	
    
    


class KsaStdMod( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='???')

	self.OptionParser.add_option('--title',
					action='store', type='string',	
					dest='title', 
					help='???')

	self.OptionParser.add_option('--place',
					action='store', type='string',	
					dest='place', 
					help='???')

	self.OptionParser.add_option('--type',
					action='store', type='string',
					dest='mtype', default='',
					help='Type of host')

	self.OptionParser.add_option('--gen_ts',
					action='store', type='inkbool',
					dest='gen_ts',
					help='Type of host')

    def effect(self):
	logic = unicode("RMS:StdModul")
	klass = unicode("RMS:StdModulClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_simple( layer, logic, klass, unicode(self.options.name), unicode(self.options.mtype), self.options.gen_ts, unicode(self.options.title), unicode(self.options.place) )

if __name__ == '__main__':
    e = KsaStdMod()
    e.affect()
