#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle, random

def draw_lp(parent, logic, klass, place, addr ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'place': place, inksa.prefix + 'addr': addr  }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = {'x': '0', 'y': '0', 'width': '40', 'height': '30', inksa.prefix + 'subid': '_bound', 'style': 'fill:none;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

    attribs = {'x': '20', 'y': '10', 'text-anchor': 'middle', inksa.prefix + 'subid': '_txt', 'style': 'font-size:8px;font-weight:lighter;letter-spacing:0.2px;word-spacing:0px;text-anchor:middle;stroke:#000000' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = place

    attribs = { 'cx': '5', 'cy': '-6', 'r':'5', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_link','d':'m 20,0 0,-15', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        
    attribs = { inksa.prefix + 'subid': '_m1','d':'m 14,13 0,15 15,-8 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    attribs = { inksa.prefix + 'subid': '_m2','d':'M 14,18 9,18', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    attribs = { inksa.prefix + 'subid': '_m3','d':'M 14,23 9,23', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    attribs = { inksa.prefix + 'subid': '_m4','d':'m 29,20 5,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    return group	


dev_dict={"uns":u"УНС4и", "ak":u"АК6д2", "umv":u"УМВ32"}
def draw_dev(parent, logic, klass, lp_place, lp_addr, matrix, mtype, gr_ip ):
    attribs = {inksa.prefix + 'lp_id': gr_ip, 'transform':matrix, inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'lp_place': lp_place, inksa.prefix + 'lp_addr': lp_addr, inksa.prefix + 'type_eng': mtype, inksa.prefix + 'type_rus': dev_dict[mtype] }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    attribs = {'x': '0', 'y': '0', 'width': '40', 'height': '30', inksa.prefix + 'subid': '_bound', 'style': 'fill:none;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

    attribs = {'x': '20', 'y': '10', 'text-anchor': 'middle', inksa.prefix + 'subid': '_txt', 'style': 'font-size:8px;font-weight:lighter;text-anchor:middle;stroke:#000000;letter-spacing:0.5px' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = dev_dict[mtype]

    attribs = { 'cx': '5', 'cy': '-6', 'r':'5', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    if  "ak" == mtype:
	attribs = { inksa.prefix + 'subid': '_link','d':'m 20,0 0,-15', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 5,22 10,0 10,-5', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m2','d':'m 25,22 10,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m3','d':'m 27,25 0,-10 -14,0 0,10 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	
    if  "uns" == mtype:
	attribs = { inksa.prefix + 'subid': '_link','d':'m 20,0 0,-18 -25,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 14,13 0,15 15,-8 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m2','d':'M 14,18 9,18', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m3','d':'M 14,23 9,23', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m4','d':'m 29,20 5,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    if  "umv" == mtype:
	attribs = { inksa.prefix + 'subid': '_link','d':'m 20,0 0,-15', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	attribs = { inksa.prefix + 'subid': '_m1','d':'m 14,13 0,15 15,-8 z', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m2','d':'M 14,18 9,18', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m3','d':'M 14,23 9,23', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
        attribs = { inksa.prefix + 'subid': '_m4','d':'m 29,20 5,0', 'style': 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    return group
    
    


class KsaCornLpFabric( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--place',
					action='store', type='string',	
					dest='place', default='-100', 
					help='???')

	self.OptionParser.add_option('--addr',
					action='store', type='string',
					dest='addr', default='-100',
					help='Type of host')

    def effect(self):
	logic = unicode("corn:MasterLp")
	klass = unicode("corn:MasterLpClass")
	group = draw_lp( self.current_layer, logic, klass, unicode(self.options.place), unicode(self.options.addr) )

	old_id = "g"+random.choice('0123456789')+random.choice('0123456789')+random.choice('0123456789')+random.choice('0123456789')
	gr_id = self.uniqueId( old_id )
	group.set('id', gr_id)

	logic = unicode("corn:MasterDev")
	klass = unicode("corn:MasterDevClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_dev( layer, logic, klass, unicode(self.options.place), unicode(self.options.addr), 'translate(45,32.362183)', "uns", gr_id )
	    draw_dev( layer, logic, klass, unicode(self.options.place), unicode(self.options.addr), 'translate(45,77.362183)', "ak", gr_id )
    	    draw_dev( layer, logic, klass, unicode(self.options.place), unicode(self.options.addr), 'translate(45,122.36218)', "umv", gr_id )
	


if __name__ == '__main__':
    e = KsaCornLpFabric()
    e.affect()
