#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_simple(parent, logic, klass, name ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'name': name }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = {'x': '3', 'y': '3', 'width': '184', 'height': '110', 'style': 'fill:none;stroke:#000000;stroke-width:3;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

#    style = {'fill:none;stroke:#000000;stroke-width:5;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none'}
#    attribs = {'x': '24', 'y': '35', 'width': '145', 'height': '24',  'style': 'fill:none;stroke:#000000;stroke-width:5;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none'}
#    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )
    
#    attribs = {'x': '35', 'y': '51', 'text-anchor': 'start', inksa.prefix + 'subid': '_txtb', 'style': 'font-size:16px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:#000000;font-family:Bitstream Vera Sans' }
#    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( 'Ручн.вкл.напр.' )

    attribs = {'x': '90', 'y': '25', 'text-anchor': 'middle', inksa.prefix + 'subid': 'name', 'style': 'font-size:16px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name
#Напряжение
    attribs = {'x': '93', 'y': '62', 'text-anchor': 'middle', inksa.prefix + 'subid': 'volt_metter_time', 'style': 'font-size:10px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( 'Время измерения' )

    attribs = {'x': '70', 'y': '47', 'text-anchor': 'middle', inksa.prefix + 'subid': 'volt_txt', 'style': 'font-size:16px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "Напр:" )

    attribs = {'x': '144', 'y': '47', 'text-anchor': 'middle', inksa.prefix + 'subid': 'volt_val', 'style': 'font-size:16px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "0.00В" )

    attribs = {'x': '10', 'y': '32', 'width': '31', 'height': '18', inksa.prefix + 'subid': 'volt_button', 'style': 'fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

    attribs = {'x': '25', 'y': '45', 'text-anchor': 'middle', inksa.prefix + 'subid': 'volt_button_txt', 'style': 'font-size:10px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "Вкл" )
#Изоляция    
    attribs = {'x': '93', 'y': '101', 'text-anchor': 'middle', inksa.prefix + 'subid': 'isol_metter_time', 'style': 'font-size:10px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( 'Время измерения' )

    attribs = {'x': '69', 'y': '89', 'text-anchor': 'middle', inksa.prefix + 'subid': 'isol_txt', 'style': 'font-size:16px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "Изол:" )

    attribs = {'x': '144', 'y': '89', 'text-anchor': 'middle', inksa.prefix + 'subid': 'isol_val', 'style': 'font-size:16px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "1,00МОм" )

    attribs = {'x': '10', 'y': '75', 'width': '31', 'height': '18', inksa.prefix + 'subid': 'isol_button', 'style': 'fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none' }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs )

    attribs = {'x': '25', 'y': '88', 'text-anchor': 'middle', inksa.prefix + 'subid': 'isol_button_txt', 'style': 'font-size:10px;font-style:normal;font-weight:normal;text-anchor:middle;fill:#000000;stroke:none' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = unicode( "Вкл" )


class KsaRpcSignal( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='ti_name', 
					help='???')

    def effect(self):
	ti_name = unicode(self.options.ti_name)

	logic = unicode("RMS:StdObj")
	klass = unicode("RMS:StdObjClass")
	
	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_simple( layer, logic, klass, ti_name )

if __name__ == '__main__':
    e = KsaRpcSignal()
    e.affect()
