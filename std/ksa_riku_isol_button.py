#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'special':"{'type':'set_attr','getitem':'riku:TiManager','attr':'isol','value':'1'}"  }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main','d':"M 140,18 0,18 0,0 140,0 z", 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '70', 'y': '13', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = {inksa.prefix + "src":"ti_manager.isol", inksa.prefix + "dst":"green", inksa.prefix + "mod":"util:con"}
    inkex.etree.SubElement(group, inkex.addNS(inksa.prefix + 'con', 'ksa'), attribs)

    attribs = {inksa.prefix + "src":"!ti_manager.isol", inksa.prefix + "dst":"off", inksa.prefix + "mod":"util:con"}
    inkex.etree.SubElement(group, inkex.addNS(inksa.prefix + 'con', 'ksa'), attribs)

    attribs = {inksa.prefix + "mod":"RMS:StdLogEvent", inksa.prefix + "type":"event", inksa.prefix + "name":u"Измерение изоляции", inksa.prefix+"event":u"Изменение статуса" , inksa.prefix+"note":u" 2-Вкл.Авто / 1-Вкл.Все / 0-Откл " , inksa.prefix + "link_type":"other_attr", inksa.prefix + "link":"ti_manager.isol_mode" }
    inkex.etree.SubElement(group, inkex.addNS(inksa.prefix + 'event', 'ksa'), attribs)


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)

    def effect(self):
	logic = unicode("RPC:Text")
	klass = unicode("RPC:ButtonIsolClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, u"Измерить изоляцию")

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
