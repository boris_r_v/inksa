#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa

def draw_object(parent, act ):
    for i in parent:
        print  i

    return

class KsaPitCord( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--mode',
					action='store', type='string',	
					dest='act', 
					help='Action')


    def effect(self):
	act = self.options.act

	draw_object(self.current_layer, act )

if __name__ == '__main__':
    e = KsaPitCord()
    e.affect()
