#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object(parent, logic, klass, name, dec, detail, font_size ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass,  inksa.prefix + 'std_mnemo': u"АмперМетка", inksa.prefix+"name":name, inksa.prefix+"detail":detail, inksa.prefix+"decimal_places":dec  }

    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    x1 = 12 - len(name)*6    

    style = {'font-size':'8px', 'font-style':'normal','font-weight':'normal', 'fill': 'black', 'stroke':'none','font-family':'Bitstream Vera Sans', 'font-size': font_size}
    attribs = {'x':str(14-(len(name)*3)), 'y': '9', 'text-anchor': 'middle', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    style = {'font-size':'8px', 'font-style':'normal','font-weight':'normal', 'fill': 'black', 'stroke':'none','font-family':'Bitstream Vera Sans', 'font-size': font_size}
    attribs = { inksa.prefix + 'subid': 'txt','x': '35', 'y': '9', 'text-anchor': 'middle', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = "0.00"

class KsaPitSwitch( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')

	self.OptionParser.add_option('--detail',
					action='store', type='string',	
					dest='det', 
					help='')

	self.OptionParser.add_option('--decimal_places',
					action='store', type='string',	
					dest='decimal_places', 
					help='')

	self.OptionParser.add_option('--font_size',
					action='store', type='string',	
					dest='font_size', 
					help='')

    def effect(self):
	name = unicode(self.options.name)
	detail = unicode(self.options.det)
	dec = unicode(self.options.decimal_places)
	fsize = unicode(self.options.font_size)+"px"
	logic = unicode("Power:amper")
	klass = unicode("Power:amperLabelClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, name, dec, detail, fsize )

if __name__ == '__main__':
    e = KsaPitSwitch()
    e.affect()
