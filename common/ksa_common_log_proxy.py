#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, svg_id, item_id, text ):
    sid = svg_id+"@"+item_id

    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'name': text, inksa.prefix + 'sectionId': sid , inksa.prefix + 'std_mnemo': u"Лог прокси" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main','d':"M 40,18 0,18 0,0 40,0 z", 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '20', 'y': '13', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = text

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--svg_id',
					action='store', type='string',	
					dest='svg_id', 
					help='Text')
	self.OptionParser.add_option('--item_id',
					action='store', type='string',	
					dest='item_id', 
					help='Text')
	self.OptionParser.add_option('--text',
					action='store', type='string',	
					dest='text', 
					help='Text')

    def effect(self):
	text = unicode(self.options.text)
	svg_id = unicode(self.options.svg_id)
	item_id = unicode(self.options.item_id)
	logic = unicode("RPC:Text")
	klass = unicode("RPC:LogProxyClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, svg_id, item_id, text )

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
