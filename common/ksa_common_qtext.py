#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, queue, title ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': title, inksa.prefix + 'queue': queue, inksa.prefix + 'std_mnemo': u"ТекстПереключаемый" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '50', 'y': '70', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs ).text = title

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--title',
					action='store', type='string',	
					dest='title', 
					help='Hostname')

	self.OptionParser.add_option('--queue',
					action='store', type='string',	
					dest='queue', 
					help='Hostname')

	self.OptionParser.add_option('--tab',
					action='store', type='string',
					dest='tab', default='contr',
					help='Type of host')

    def effect(self):
	queue = unicode(self.options.queue)
	title = unicode(self.options.title)
	logic = unicode("RPC:QText")
	klass = unicode("RPC:QTextClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, queue, title)

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
