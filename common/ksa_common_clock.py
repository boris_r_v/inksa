#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, ftype ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': u"Часы", inksa.prefix + 'std_mnemo': u"Часы" }
    sdf = u"Текущая телесигнализация"
    if ( "self" != ftype ): 
	attribs[inksa.prefix + 'mode'] = "1"
	
    else:
        sdf = u"Текущее время"
	attribs[inksa.prefix + 'mode'] = "0"

    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_date', 'x': '54', 'y': '71', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u"00/00/00"

    attribs = { inksa.prefix + 'subid': '_time', 'x': '136', 'y': '71', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = u"00:00:00"

    attribs = { inksa.prefix + 'subid': '_state', 'x': '90', 'y': '41', 'text-anchor': 'middle',  'style': 'font-size:18px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = sdf


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)

	self.OptionParser.add_option('--tab',
					action='store', type='string',	
					dest='tab', 
					help='')

	self.OptionParser.add_option('--type',
					action='store', type='string',	
					dest='type', 
					help='')


    def effect(self):
	logic = unicode("RPC:Clock")
	klass = unicode("RPC:ClockClass")
	ftype = unicode(self.options.type)

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, ftype )

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
