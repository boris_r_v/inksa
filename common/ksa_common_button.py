#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, name, toggle):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': name, inksa.prefix + 'std_mnemo': u"Кнопка", inksa.prefix + 'timeout': "1" if not toggle else "-1" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main','d':"M 40,18 0,18 0,0 40,0 z", 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '20', 'y': '13', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')
	self.OptionParser.add_option('--toggle',
					action='store', type='inkbool',	
					dest='toggle', 
					help='Type a button')
	self.OptionParser.add_option('--mode',
					action='store', type='string',
					dest='mode', default='contr',
					help='Type of host')

    def effect(self):
	name = unicode(self.options.name)
	toggle = self.options.toggle
	logic = unicode("RPC:Button")
	klass = unicode("RPC:ButtonClass")

        #inkex.errormsg('Toggle: '+ str( toggle ) + " " + tgl )

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, name, toggle)

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
