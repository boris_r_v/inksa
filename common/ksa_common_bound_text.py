#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, text, title ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'name': text, inksa.prefix + 'title': title, inksa.prefix + 'std_mnemo': u"Текст" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { inksa.prefix + 'subid': '_main','d':"M 40,18 0,18 0,0 40,0 z", 'style': 'fill:none;stroke:blue;stroke-width:2' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': '_txt', 'x': '20', 'y': '13', 'text-anchor': 'middle',  'style': 'font-size:12px;font-weight:normal;fill:#ffffff;stroke:#ffffff;stroke-width:0' }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = text

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)


class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--text',
					action='store', type='string',	
					dest='text', 
					help='Text')
	self.OptionParser.add_option('--title',
					action='store', type='string',	
					dest='title', 
					help='Signal description')

    def effect(self):
	text = unicode(self.options.text)
	title = unicode(self.options.title)
	logic = unicode("RPC:Text")
	klass = unicode("RPC:BoundClass")

	if ( 0 == len(text) and 0 != len(title) ):
	    text = title
	if ( 0 != len(text) and 0 == len(title) ):
	    title = text

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, text, title )

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
