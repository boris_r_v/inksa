#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object(parent, logic, klass, name, valtype, size, detail, vmax, vmin, alfa, betta ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass,  inksa.prefix + 'std_mnemo': u"Ампер", inksa.prefix+"max":vmax, inksa.prefix+"min":vmin, inksa.prefix+"name":name, inksa.prefix+"detail":detail, inksa.prefix+"alfa":alfa, inksa.prefix+"betta":betta  }
#    if ( len ( detail ) ):	
#	attribs[inksa.prefix + 'tooltip'] = detail;
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    x1 = 12 - len(name)*6    

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'm1', 'd': 'M '+str(x1)+',0 '+str(x1)+',12 15,12 15,0 Z','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'M 15,0 15,12 55,12 55,0 z ','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'm3', 'd': 'M 55,0 55,12 70,12 70,0 z','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'font-size':'8px', 'font-style':'normal','font-weight':'normal', 'fill': 'black', 'stroke':'none','font-family':'Bitstream Vera Sans'}
    attribs = {'x':str(14-(len(name)*3)), 'y': '9', 'text-anchor': 'middle', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    style = {'font-size':'8px', 'font-style':'normal','font-weight':'normal', 'fill': 'black', 'stroke':'none','font-family':'Bitstream Vera Sans'}
    attribs = { inksa.prefix + 'subid': 'txt','x': '35', 'y': '9', 'text-anchor': 'middle', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = "0.00"

    style = {'font-size':'8px', 'font-style':'normal','font-weight':'normal', 'fill': 'black', 'stroke':'none','font-family':'Bitstream Vera Sans'}
    attribs = {'x': '62.5', 'y': '9', 'text-anchor': 'middle', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = valtype

    
    


class KsaPitSwitch( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')

	self.OptionParser.add_option('--valtype',
					action='store', type='string',	
					dest='vt', 
					help='Hostname')

	self.OptionParser.add_option('--size_weight',
					action='store', type='int',	
					dest='size', 
					help='Толщина линии')

	self.OptionParser.add_option('--valmax',
					action='store', type='string',	
					dest='vmax', 
					help='')

	self.OptionParser.add_option('--valmin',
					action='store', type='string',	
					dest='vmin', 
					help='')

	self.OptionParser.add_option('--detail',
					action='store', type='string',	
					dest='det', 
					help='')

	self.OptionParser.add_option('--alfa',
					action='store', type='string',	
					dest='alfa', 
					help='')

	self.OptionParser.add_option('--betta',
					action='store', type='string',	
					dest='betta', 
					help='')

    def effect(self):
	name = unicode(self.options.name)
	vt = unicode(self.options.vt)
	sz = self.options.size
	detail = unicode(self.options.det)
	vmax = unicode(self.options.vmax)
	vmin = unicode(self.options.vmin)
	alfa = unicode(self.options.alfa)
	betta = unicode(self.options.betta)
	logic = unicode("Power:amper")
	klass = unicode("Power:amperClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, name, vt, str(sz), detail, vmax, vmin, alfa, betta )

if __name__ == '__main__':
    e = KsaPitSwitch()
    e.affect()
