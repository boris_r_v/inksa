#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_way(parent, logic, klass, title, ftype ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'title': title, inksa.prefix + 'std_mnemo': u"Фигура" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': '1' }     
    if ("circle" == ftype): 
	attribs = { inksa.prefix + 'subid': '_main', 'cx': '0', 'cy':'0', 'r':'10', 'style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    if ("triangle" == ftype): 
	attribs = { inksa.prefix + 'subid': '_main', 'd': 'm 0,0 -20,0 10,20 z','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    if ("square" == ftype): 
	attribs = { inksa.prefix + 'subid': '_main', 'd': 'm 0,0 0,15 20,0 0,-15 z','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = {inksa.prefix + "src":"", inksa.prefix + "dst":"off", inksa.prefix + "mod":"Net:con"}
    inkex.etree.SubElement(group, inkex.addNS('con', 'ksa'), attribs)



class KsaRpcWay( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--tab',
					action='store', type='string',	
					dest='', 
					help='')

	self.OptionParser.add_option('--title',
					action='store', type='string',	
					dest='title', 
					help='')

	self.OptionParser.add_option('--figure_type',
					action='store', type='string',	
					dest='figure_type', 
					help='')

    def effect(self):
	title = unicode(self.options.title)
	ftype = unicode(self.options.figure_type)
	logic = unicode("RPC:Figure")
	klass = unicode("RPC:FigureClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_way( layer, logic, klass, title, ftype )

if __name__ == '__main__':
    e = KsaRpcWay()
    e.affect()
