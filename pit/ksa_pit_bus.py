#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object( parent, logic, klass, size, volt, lenght ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "0", inksa.prefix + 'uni_type': "8" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    path = "M 0 10 " + str(int(lenght)*20) + " 10"
    attribs = { inksa.prefix + 'subid': 'main', 'd': path,'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    for s in  range( int(lenght) ):
	num_point = str(s+1)
	cx = str(10 + s*20)
	attribs = { 'cx': cx, 'cy': '10', 'r':'3.3', inksa.prefix + 'title': num_point, inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
	inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)


class KsaPitCord( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--size_weight',
					action='store', type='string',	
					dest='size', 
					help='Толщина линии')
	self.OptionParser.add_option('--volt',
					action='store', type='string',	
					dest='volt', 
					help='Напрядение на блоке')

	self.OptionParser.add_option('--lenght',
					action='store', type='string',	
					dest='lenght', 
					help='Длина шины')



    def effect(self):
	logic = unicode("Power:gosser")
	klass = unicode("Power:cordClass")
	size = self.options.size
	volt = self.options.volt
	lenght = self.options.lenght

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, size, volt, lenght )


if __name__ == '__main__':
    e = KsaPitCord()
    e.affect()
