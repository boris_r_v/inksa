#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object( parent, logic, klass, volt, move = None ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "0", inksa.prefix + 'uni_type': "8" }
    if (None != move ):
	attribs["transform"] = move
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': '5px'} 
    attribs = { inksa.prefix + 'subid': 'main', 'cx': '0', 'cy': '0', 'r':'2', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)
    r='3'
    attribs = { 'cx': '0', 'cy': '6', 'r':r, inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '0', 'cy': '-6', 'r': r, inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '6', 'cy': '0', 'r': r, inksa.prefix + 'title': '3', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '-6', 'cy': '0', 'r': r, inksa.prefix + 'title': '4', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)


class KsaPitCord( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--volt',
					action='store', type='string',	
					dest='volt', 
					help='Напрядение на блоке')

    def effect(self):
	logic = unicode("Power:gosser")
	klass = unicode("Power:cordClass")
	volt = self.options.volt

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, volt )


if __name__ == '__main__':
    e = KsaPitCord()
    e.affect()
