#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object(parent, logic, klass, name, first_type, second_type, size, volt, volt2):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'v2_type': volt2, inksa.prefix + 'm_type': "0", inksa.prefix + 'uni_type': "8" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'c1', 'cx': '0', 'cy':'0', 'r':'10', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'c2', 'cx': '15', 'cy':'0', 'r':'10', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'm1', 'd': 'M -20,0 -10,0','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'm2', 'd': 'M 25,0 35,0','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'blue', 'stroke': 'blue'}
    attribs = {'x': '27', 'y': '-5', 'text-anchor': 'middle', inksa.prefix + 'subid': 'txt', 'style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name
    
#    if first_type == 'triangle':
#        style = {'fill': 'none', 'stroke':'black', 'stroke-width':'1px' }
#	attribs = { 'd': 'M 16.517856,24.642857 9.178571,12.553572 1.5892857,24.607143 z','style': simplestyle.formatStyle(style) }
#        inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
#    elif first_type == 'star':
#        style = {'fill': 'none', 'stroke':'black', 'stroke-width':'1px' }
#	attribs = { 'd': 'm 8.4937448,19.747462 5.8041462,6.090803 m -5.8041462,-6.090803 0.017059,-6.705089 m -0.017059,6.705089 -6.2170719,6.056684', 'style': simplestyle.formatStyle(style) }
#        inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
#
#    if second_type == 'triangle':
#	style = {'fill': 'none', 'stroke':'black', 'stroke-width':'1px' }
#	attribs = { 'd': 'm 43.749998,25.267855 -14.910713,10e-7 7.232142,-12.142857 z','style': simplestyle.formatStyle(style) }
#        inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
#    elif second_type == 'star':
#        style = {'fill': 'none', 'stroke':'black', 'stroke-width':'1px' }
#	attribs = { 'd': 'm 36.231414,20.163253 5.982717,6.312704 m -5.982717,-6.312704 0.195631,-6.861997 m -0.195631,6.861997 -5.807626,6.374215','style': simplestyle.formatStyle(style) }
#        inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { 'cx': '-7', 'cy': '0', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '23', 'cy': '0', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

class KsaPitTrans3( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')

	self.OptionParser.add_option('--size_weight',
					action='store', type='string',	
					dest='size', 
					help='Толщина линии')

	self.OptionParser.add_option('--volt',
					action='store', type='string',	
					dest='volt', 
					help='Напрядение на блоке')

	self.OptionParser.add_option('--volt2',
					action='store', type='string',	
					dest='volt2', 
					help='Напрядение на блоке')

        self.OptionParser.add_option('--first_type',
                                       action='store', type='string',
                                       dest='first_type', default='star',
                                       help='Type of first connections')
 
        self.OptionParser.add_option('--second_type',
                                       action='store', type='string',
                                       dest='second_type', default='star',
                                       help='Type of second connections')


    def effect(self):
	name = unicode(self.options.name)
	size = unicode(self.options.size)
	volt = unicode(self.options.volt)
	volt2 = unicode(self.options.volt2)
        first_t = unicode(self.options.first_type)
        second_t = unicode(self.options.second_type)
	
	logic = unicode("Power:gosser")
	klass = unicode("Power:transClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, name, first_t, second_t, str(size), volt, volt2 )

if __name__ == '__main__':
    e = KsaPitTrans3()
    e.affect()
