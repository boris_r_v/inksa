#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object(parent, logic, klass, size, volt, big ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "2", inksa.prefix + 'uni_type': "8" }
    group = inkex.etree.SubElement( parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap )

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'main', 'd': 'M 0 5 L 34 5 L 34 39 L 0 39 L 0 5 L 5 5 L 5 0 L 39 0 L 39 34 L 34 34 L 34 39','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'main1', 'd': 'M 20 10 L 20 34','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'main2', 'd': 'M 17 15 L 17 29','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    if ( True == big ):
	style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
	attribs = { inksa.prefix + 'subid': 'main4', 'd': 'M 0 49 L 34 49 L 34 83 L 0 83 L 0 49 L 5 49 L 5 44 L 39 44 L 39 78 L 34 78 L 34 83','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

	style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
	attribs = { inksa.prefix + 'subid': 'main5', 'd': 'M 20 54 L 20 78','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

	style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
	attribs = { inksa.prefix + 'subid': 'main6', 'd': 'M 17 59 L 17 73','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

	style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
	attribs = { inksa.prefix + 'subid': 'main7', 'd': 'M 17 22 L -15 22 L -15 66 L 17 66 L -15 66 L -15 22 L -25 22 L -40 14 L -40 30 L -25 22','style': simplestyle.formatStyle(style) }
	inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
    else:
	style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
	attribs = { inksa.prefix + 'subid': 'main4', 'd': 'm 17,21.362183 -39.857143,0.07143 -15,-8 0,16 15,-8','style': simplestyle.formatStyle(style) }
        inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)
	
    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'main3', 'd': 'm -85.01826,21.369816 25,0 15,-8 0,16 -15,-8','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'key', 'd': 'm -42.936865,12.35203 0,18 3,0 0,-18 z','style': 'fill:none;stroke:#0000ff;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1' }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { 'cx': '-75', 'cy': '22', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    fail_attrs = { inksa.prefix + 'subid': "fail", "transform":"translate(47,11)" }
    fails = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), fail_attrs, inksa.nsmap)
    attribs = { 'cx': '0', 'cy': '0', 'r':'6', 'style':"fill:#666666;stroke:yellow;stroke-width:1;stroke-opacity:1;fill-opacity:1;" }
    inkex.etree.SubElement(fails, inkex.addNS('circle', 'svg'), attribs)
    attribs = {'x': '0', 'y': '4', 'text-anchor': 'middle', 'style': 'fill:yellow;stroke:yellow;font-size:11px;font-family:Bitstream Vera Sans' }
    inkex.etree.SubElement(fails, inkex.addNS('text', 'svg'), attribs).text = u"!"

    crash_attrs = { inksa.prefix + 'subid': "crash", "transform":"translate(-8,11)" }
    crash = inkex.etree.SubElement(group, inkex.addNS('g', 'svg'), crash_attrs, inksa.nsmap)
    attribs = { 'cx': '0', 'cy': '0', 'r':'6', 'style':"fill:#666666;stroke:#ff0000;stroke-width:1;stroke-opacity:1;fill-opacity:1;" }
    inkex.etree.SubElement(crash, inkex.addNS('circle', 'svg'), attribs)
    attribs = {'x': '0', 'y': '4', 'text-anchor': 'middle', 'style': 'fill:#ff0000;stroke:#ff0000;font-size:11px;font-family:Bitstream Vera Sans' }
    inkex.etree.SubElement(crash, inkex.addNS('text', 'svg'), attribs).text = u"!"


class KsaPitCord( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--size_weight',
					action='store', type='string',	
					dest='size', 
					help='Толщина линии')
	self.OptionParser.add_option('--volt',
					action='store', type='string',	
					dest='volt', 
					help='Напрядение на блоке')


    def effect(self):
	logic = unicode("Power:gosser")
	klass = unicode("Power:batClass")
	size = self.options.size
	volt = self.options.volt
	big = False

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, size, volt, big )

if __name__ == '__main__':
    e = KsaPitCord()
    e.affect()
