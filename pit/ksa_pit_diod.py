#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle

def draw_object(parent, logic, klass, size, volt):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "3", inksa.prefix + 'uni_type': "8" }
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)
    
    style = {'fill': 'none', 'stroke':'blue', 'stroke-width': size+'px' } 
    attribs = { inksa.prefix + 'subid': 'main', 'd': 'M 0 15 L 60 15 L 40 0 L 40 30 L 60 15 L 60 0 L 60 30 L 60 15 L 100 15','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { 'cx': '10', 'cy': '15', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '90', 'cy': '15', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)


class KsaPitCord( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--size_weight',
					action='store', type='string',	
					dest='size', 
					help='Толщина линии')

	self.OptionParser.add_option('--volt',
					action='store', type='string',	
					dest='volt', 
					help='Напрядение на блоке')

    def effect(self):
	logic = unicode("Power:gosser")
	klass = unicode("Power:cordClass")
	size = self.options.size
	volt = self.options.volt

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, size, volt )

if __name__ == '__main__':
    e = KsaPitCord()
    e.affect()
