#!/usr/bin/env python
#-*- coding: utf-8 -*-

import inkex, inksa, simplestyle
import ksa_pit_dot

def draw_legacy( parent, logic, klass, name, size, volt, move = None ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "1", inksa.prefix + 'title': name, inksa.prefix + 'uni_type': "8"  }
    if (None != move ):
	attribs["transform"] = move
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    style = {'fill': 'none', 'stroke':'black'}
    attribs = {'x': '30', 'y': '8', 'width': '35', 'height': '27', inksa.prefix + 'subid': 'bound', 'style': simplestyle.formatStyle(style)  }
    inkex.etree.SubElement(group, inkex.addNS('rect', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'dot1x', 'd': 'M 0 27 L 35 27','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'dot2x', 'd': 'M 62 24 L 62 30 L 62 27 L 95 27','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'zero', 'd': 'M 35 27 L 60 12','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'one', 'd': 'M 35 27 L 62 27','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style_t = {'fill': 'blue', 'stroke': 'none', 'font-size':'8px'}
    attribs = {'x': '47', 'y': '5', 'text-anchor': 'middle', inksa.prefix + 'subid': 'txt', 'style': simplestyle.formatStyle(style_t) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name

    attribs = { 'cx': '10', 'cy': '27', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '85', 'cy': '27', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

def draw_nc( parent, logic, klass, name, size, volt, move = None ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "1", inksa.prefix + 'title': name, inksa.prefix + 'uni_type': "8"  }
    if (None != move ):
	attribs["transform"] = move
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { 'cx': '10', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot1', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot3', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'dot1x', 'd': 'M 10 40 L 5 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'dot3x', 'd': 'M 30 45 L 30 40 L 35 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'zero', 'd': 'M 10 40 L 31 44','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'one', 'd': 'M 10 40 L 29 51','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style_t = {'fill': 'blue', 'stroke': 'none', 'font-size':'8px'}    
    attribs = {'x': '25', 'y': '30', 'text-anchor': 'middle', inksa.prefix + 'subid': 'txt', 'style': simplestyle.formatStyle(style_t) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name
    
    attribs = { 'cx': '10', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '3', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)


def draw_no( parent, logic, klass, name, size, volt, move = None ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "1", inksa.prefix + 'title': name, inksa.prefix + 'uni_type': "8"  }
    if (None != move ):
	attribs["transform"] = move
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { 'cx': '10', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot1', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot2', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'dot1x', 'd': 'M 10 40 L 5 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'dot2x', 'd': 'M 30 40 L 35 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'one', 'd': 'M 10 40 L 30 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'zero', 'd': 'M 10 40 L 31 29','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style_t = {'fill': 'blue', 'stroke': 'none', 'font-size':'8px'}    
    attribs = {'x': '25', 'y': '30', 'text-anchor': 'middle', inksa.prefix + 'subid': 'txt', 'style': simplestyle.formatStyle(style_t) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name
    
    attribs = { 'cx': '10', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

def draw_nco( parent, logic, klass, name, size, volt, move = None ):
    attribs = { inksa.prefix + 'logic': logic, inksa.prefix + 'class': klass, inksa.prefix + 'v_type': volt, inksa.prefix + 'm_type': "1", inksa.prefix + 'title': name, inksa.prefix + 'uni_type': "8"  }
    if (None != move ):
	attribs["transform"] = move
    group = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inksa.nsmap)

    attribs = { 'cx': '10', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot1', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)
    attribs = { 'cx': '30', 'cy': '40', 'r':'1', inksa.prefix + 'subid': 'dot2', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)
    attribs = { 'cx': '30', 'cy': '25', 'r':'1', inksa.prefix + 'subid': 'dot3', 'style':"fill:none;stroke:blue;stroke-width:3;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    style = {'fill': 'none', 'stroke':'blue', 'stroke-width':size+'px' }
    attribs = { inksa.prefix + 'subid': 'dot1x', 'd': 'M 10 40 L 5 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'dot2x', 'd': 'M 30 40 L 35 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'dot3x', 'd': 'M 30 30 L 30 25 L 35 25','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'one', 'd': 'M 10 40 L 30 40','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    attribs = { inksa.prefix + 'subid': 'zero', 'd': 'M 10 40 L 31 29','style': simplestyle.formatStyle(style) }
    inkex.etree.SubElement(group, inkex.addNS('path', 'svg'), attribs)

    style_t = {'fill': 'blue', 'stroke': 'none', 'font-size':'8px'}    
    attribs = {'x': '40', 'y': '20', 'text-anchor': 'middle', inksa.prefix + 'subid': 'txt', 'style': simplestyle.formatStyle(style_t) }
    inkex.etree.SubElement(group, inkex.addNS('text', 'svg'), attribs).text = name
    
    attribs = { 'cx': '10', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '1', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '40', 'r':'3.3', inksa.prefix + 'title': '2', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

    attribs = { 'cx': '30', 'cy': '25', 'r':'3.3', inksa.prefix + 'title': '3', inksa.prefix + 'mode': 'link-point', 'style':"fill:none;stroke:#ebe417;stroke-width:1;stroke-opacity:1;fill-opacity:1" }
    inkex.etree.SubElement(group, inkex.addNS('circle', 'svg'), attribs)

def draw_avr( parent, logic, klass, name, size, volt ):
    draw_no( parent, logic, klass, name, size, volt, "translate(40,6.362207)" )
    draw_nc( parent, logic, klass, name, size, volt, "translate(40,-16.637794)" )
    ksa_pit_dot.draw_object( parent, unicode("Power:gosser"), unicode("Power:cordClass"), volt, "translate(85,23.362206)" )


def draw_object(parent, logic, klass, name, size, volt, type):
	    
    if ( "OLD" == type ):
	draw_legacy( parent, logic, klass, name, size, volt )

    elif ( "NC" == type ):
	draw_nc( parent, logic, klass, name, size, volt )

    elif ( "NO" == type ):
	draw_no( parent, logic, klass, name, size, volt )
    
    elif ( "NCO" == type ):
	draw_nco( parent, logic, klass, name, size, volt )

    elif ( "AVR" == type ):
	draw_avr( parent, logic, klass, name, size, volt )


class KsaPitSwitch( inksa.Effect ):
    def __init__(self):
	inksa.Effect.__init__(self)
	self.OptionParser.add_option('--name',
					action='store', type='string',	
					dest='name', 
					help='Hostname')

#	self.OptionParser.add_option('--size_weight',
#					action='store', type='int',	
#					dest='size', 
#					help='Толщина линии')

	self.OptionParser.add_option('--volt',
					action='store', type='int',	
					dest='volt', 
					help='Напрядение на блоке')

	self.OptionParser.add_option('--type',
					action='store', type='string',	
					dest='type', 
					help='Тип ключа')

	self.OptionParser.add_option('--tab',
					action='store', type='string',	
					dest='tab', 
					help='')



    def effect(self):
	name = unicode(self.options.name)
	size = "2" #unicode(self.options.size)
	volt = unicode(self.options.volt)
	type = unicode(self.options.type)
	logic = unicode("Power:gosser")
	klass = unicode("Power:switchClass")

	layer = self.draw_layer( self.current_layer )
	if layer is not None:
	    draw_object( layer, logic, klass, name, size, volt, type )

if __name__ == '__main__':
    e = KsaPitSwitch()
    e.affect()
