#!/usr/bin/env python
#-*- coding: utf-8 -*- 

import sys
reload(sys)
sys.setdefaultencoding("UTF-8")

import inkex, simplestyle

#DEPRECETED! Don't use this variable
uri = 'http://nilksa.ru/namespaces/ksa_inkscape_extension'
prefix = '{' + uri + '}'
nsmap = {'ksa': uri}

inkex.NSS[u'ksa'] = u'http://nilksa.ru/namespaces/ksa_inkscape_extension'

class EffectBase(inkex.Effect) :

    def __init__(self):
	inkex.Effect.__init__(self)
	self.OptionParser.add_option('--logic',
					action='store', type='string',	
					dest='logic',
					help='Ksa logic element')
    def load_markers(self):
	defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs == None:
            defs = inkex.etree.SubElement(self.document.getroot(), inkex.addNS('defs','svg'))

	mark_id = 'KsaUCMarker'
	for child in defs:
	    if child.get('id') == mark_id:
		return

	attrib = {  'id': mark_id,
		    'orient': 'auto',
		    'refX': '0.0',
		    'refY': '0.0',
		    'style': 'overflow:visible'
	}
	marker = inkex.etree.SubElement(defs, inkex.addNS('marker','svg'), attrib)

	style = {   
		    'fill-rule': 'evenodd',
		    'fill': '#ffffff',
		    'stroke': '#000000',
		    'stroke-width': '1.0pt'
	}
	attribs = {  
		    'd': 'M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z',
		    'transform': 'scale(0.8)',
		    'style': simplestyle.formatStyle(style)
	}
	inkex.etree.SubElement(marker, inkex.addNS("path", 'svg'), attribs)

class Effect(EffectBase) :

    def __init__(self):
	EffectBase.__init__(self)
	self.OptionParser.add_option('--class',
					action='store', type='string',	
					dest='klass',
					help='Ksa graphical class')

    def draw_layer( self, curent_layer ):
	if 'layer1' == curent_layer.get('id'):
	    return curent_layer

	root = self.document.getroot()	
	layer = root.find("./svg:g[@id='layer1']", inkex.NSS )
	if layer is None:
	    sys.stderr.write( "You are not in the drawing layer (layer1).\nAutomatic return to drawing layer was unsuccessful.\nPlease do it manual." )
	return layer



class RPC_Sections():
    def __init__(self):
	return	


def draw_layer( curent_layer ):
    if 'layer1' == curent_layer.get('id'):
        return curent_layer

    layer = curent_layer.getparent()
    while layer is not None:
	if 'layer1' == layer.get('id'):
	    return layer
	layer = layer.getparent()
    sys.stderr.write( "You are not in the drawing layer (layer1).\nAutomatic return to drawing layer was unsuccessful.\nPlease do it manual." )
    return None        

def service_layer(parent, name):
    layer = None
    for child in parent:
	if child.get('{%s}label' % inkex.NSS['inkscape']) == name:
	    layer = child
	    parent.remove(layer)
	    parent.append(layer)
	    break

    if layer == None:
	attribs = { 
		    '{%s}mode' % inkex.NSS['ksa']: 'service',
		    '{%s}label' % inkex.NSS['inkscape']: name, 
		    '{%s}groupmode' % inkex.NSS['inkscape']: 'layer'
	}
	layer = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'), attribs, inkex.NSS)
    return layer
